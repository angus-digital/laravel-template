<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Angus Digital - Template Project

This repo can be used as a base installation for working on a new Laravel application, similar to what you would get with a `laravel new` installation.

It provides some conventions that are used across projects, and adds some external dependencies such as [`laravel-breadcrumbs`](https://packagist.org/packages/davejamesmiller/laravel-breadcrumbs).

You can learn more about the code style and conventions [here](https://style.angus.digital).

## What's in the box?

Some of the highlights of what's included here:

- Devtools: Laravel Debugbar, Laravel mail preview
- Database backups
- Sentry integration
- PHP-CS fixer
- Envoy deploy script

## Usage

### Easy installation

To create a new project using this Laravel template:

```bash
composer create-project angus-digital/template
```

### Manual installation

Download the master branch

```bash
git clone https://gitlab.com/angus-digital/template.git
```

Install dependencies

```bash
composer install
```

Setup environment + run migrations

```bash
cp .env.example .env
php artisan key:generate
php artisan migrate --seed
```

### Assets

Install front end dependencies

```bash
yarn
```

Compile front end assets

```bash
yarn dev
```

Available build tasks are defined in `package.json`

## Contributing

Thank you for considering contributing to the Angus Digital Laravel Template! The contribution guide can be found in [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

This project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
